﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component1
{
    public class Component1 : AbstractComponent
    {
        public IObsluga component2;
        public Component1()
        {
            this.RegisterRequiredInterface(typeof(IObsluga));
        }
        public void wybierzMycie()
        {
            Console.WriteLine("Wybieram mycie");
        }

        public void wyswietluslugi()
        {
            Console.WriteLine("Oferowane Uslugi...");
        }



        public void myj()
        {
            Console.WriteLine("Myju, Myju po 1 komponencie");
        }

        public void jedz()
        {
            Console.WriteLine("Jezdze po 1 komponencie");
        }
        public override void InjectInterface(Type type, object impl)
        {
            if (type == typeof(IObsluga))
            {
                component2 = (IObsluga)impl;
            }
        }
    }
}
