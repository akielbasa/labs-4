﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;
using Lab4.Component2B;

namespace Lab4.MainB
{
    public class Program
    {
        static void Main(string[] args)
        {
            Component1.Component1 pierwszy = new Component1.Component1();
            //Component2.Component2 drugi = new Component2.Component2();
            Component2B.Component2B trzeci = new Component2B.Component2B();
            //Container kontener = new Container();
            //kontener.RegisterComponents(new IComponent[3] { pierwszy, drugi, trzeci });
            pierwszy.myj();
            pierwszy.jedz();
            //((IObsluga)drugi).myj();
            //((IObsluga)drugi).jedz();
            ((IObsluga)trzeci).myj();
            ((IObsluga)trzeci).jedz();
            ((IOferowaneUslugi)trzeci).wyswietluslugi();
            ((IOferowaneUslugi)trzeci).wybierzMycie();
            Console.ReadKey();

        }
    }
}
