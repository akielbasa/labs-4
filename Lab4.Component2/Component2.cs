﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Component2 : AbstractComponent, IObsluga
    {
        public Component2()
        {
            this.RegisterProvidedInterface(typeof(IObsluga), this);
        }

        void IObsluga.myj()
        {
            Console.WriteLine("Myje");
        }

        void IObsluga.jedz()
        {
            Console.WriteLine("Jade");
        }
        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

    }
}
