﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Component2B : AbstractComponent, IObsluga, IOferowaneUslugi
    {
        public Component2B()
        {
            this.RegisterProvidedInterface(typeof(IObsluga), this);
        }

        void IObsluga.myj()
        {
            Console.WriteLine("Myju w Component2B");
        }

        void IObsluga.jedz()
        {
            Console.WriteLine("Jade w Component2B");
        }

        void IOferowaneUslugi.wybierzMycie()
        {
            Console.WriteLine("Wybieram w Component2B");
        }

        void IOferowaneUslugi.wyswietluslugi()
        {
            Console.WriteLine("Wyswietlam w Component2B");
        }
        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
